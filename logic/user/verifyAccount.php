<?php

//Load composer's autoloader
require_once __DIR__ . '/../../vendor/autoload.php';

use Respect\Validation\Validator as v;

$inputEmail = $_REQUEST['inputEmail_a'];
$inputCode = $_REQUEST['inputCode'];

if (!isset($inputEmail) OR empty($inputEmail)) {
  header('Location: /index.php?alertReason=verifyAccount_isset_email');
  die();
} else {
  if(!(v::email()->validate($inputEmail))) {
    header('Location: /index.php?alertReason=verifyAccount_no_email');
    die();
  }
}

if (!isset($inputCode) OR empty($inputCode)) {
  header('Location: /index.php?alertReason=verifyAccount_isset_code');
  die();
}

if(!(v::intVal()->between(100000, 999999)->validate($inputCode))) {
  header('Location: /index.php?alertReason=verifyAccount_unsuccessful');
  die();
}

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT code FROM users WHERE email = :email;');
$stmt->bindParam(':email', $inputEmail);
$stmt->execute();

$codeInDatabase = '';
while ($row = $stmt->fetch()) {
  $codeInDatabase = $row[0];
  break;
}

if($codeInDatabase == $inputCode) {
  $stmt = $conn->prepare('UPDATE users SET confirmed = 1 WHERE email = :email;');
  $stmt->bindParam(':email', $inputEmail);
  $stmt->execute();

  $stmt = $conn->prepare('UPDATE users SET code = 0 WHERE email = :email;');
  $stmt->bindParam(':email', $inputEmail);
  $stmt->execute();

  header('Location: /index.php?alertReason=verifyAccount_successful');
  die();
} else {
  header('Location: /index.php?alertReason=verifyAccount_unsuccessful');
  die();
}