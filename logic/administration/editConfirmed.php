<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 06.08.17
 * Time: 18:00
 */

include('../ifNotLoggedInRedirectToIndex.php');
include('../ifNotEnoughPermissionRedirectToIndex.php');

if (!isset($_REQUEST['UUID']) OR empty($_REQUEST['UUID'])) {
  header('Location: /userManagement.php?alertReason=editRank_isset_UUID');
  die();
} else {
  if (!is_numeric($_REQUEST['UUID'])) {
    header('Location: /index.php?alertReason=editRank_isset_UUID');
    die();
  }
}

$UUID = $_REQUEST['UUID'];

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT confirmed FROM users WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $confirmed = $row[0];
  break;
}

$toChange = 1;
if($confirmed == 1) {
  $toChange = 0;
}

$stmt = $conn->prepare('UPDATE users SET confirmed = :confirmed WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->bindParam(':confirmed', $toChange);
$stmt->execute();

header('Location: /userManagement.php?alertReason=editVerified_successful');
die();