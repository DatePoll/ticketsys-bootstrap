<?php

/*
 * All functions which are located here are also in realkeys.php (which you have to create)
 * Enter your keys in realkeys.php so you have no pull conflicts
 */

/*

function getDatabaseHost() {
  return '';
}

function getDatabaseName() {
  return '';
}

function getDatabaseUser() {
  return '';
}

function getDatabasePassword() {
  return '';
}

function getEmailHost() {
  return '';
}

function getEmailUsername() {
  return '';
}

function getEmailPassword() {
  return '';
}

function getEmailPort() {
  return 0;
}

function getContactEmailAddress() {
	return '';
}

*/

require_once(__DIR__.'/realkeys.php');