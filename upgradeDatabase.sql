-- After 0.7.3
ALTER TABLE users CHANGE rank rank ENUM('administrator','moderator', 'user') NOT NULL;